# A transitional with humanist axes

The formative principle of this font is to make a font
by following Karen Cheng's book _designing type_.
`akern` is an anagram of Karen.

I feel like i want something like Janson, but with a small obliqueness.
Other inspirations / references are Mrs. Eaves, Galliard, Guardi, and
Baskerville.

Basic stroke dimensions.

x-height is 66.7% of cap-height

Width of vertical stem in O is 18.7% of its height
Horizon stem has width set at 5:1, so 3.7% of cap height

With a 750 unit cap height, that gives:
- cap: 750
- x-height: 500

Dimensions controlling capitals:
- O-thick: 140
- O-thin: 28
- E-thick: 118 (84.6% of O-thick)
- 1% of cap: 7.5
- 2% of cap: 15
- O obliqueness: 4 degree to the left
- serif taper: 22

Dimensions controlling lower case (see below for sketch notes):
- o-thick: 112
- o-thin: 24
- l-thick: 98
- 2% of cap = 15 = 3% of x-height
- serif taper: 20

Notes from _designing_:
- optical overshoot on **o** is in absolute terms the same as **O**.
- o-thick is 81% of O-thick (= 113/140)
- l-thick is 83% of E-thick (= 98/118, 86.7% of o-thick)
- o-thin is 84% of O-thin (= 24/28)

## Eval 2023 (sometime before 2023-08)

- on /a move NW part of bowl NW
- on /g triple underline (revise loop?)
- on /p /q longer descender


## Critique 2021-06-17

### g

Seems weak overall. Cheng says not to project the spur out to the
right too far, it effects spacing, but i think it looks weak.
The bottom loop seems to light. Should we enlarge it (to bring
it closer to the top loop and make it wider)?

### f

good. Consider adding more weight to teardrop.


### e

The thinning of the crossbar towards the left seems overly cute.
I like the teardrop shaped eye, causing a buildup of ink at the
right of the crossbar.

bottom of bowl seems... wonky?


### j

can we see a variant with a teardrop.


### o

fine


### v

fine

### w

fine. good use of Cheng's advice to avoid a third serif.


### z

it's too narrow and the serifs are too light


### r

fine apart from that horrific teardrop.


### l

fine, but the serif looks a little bit like a flag.

### m

good. I appreciate the change in the spring, but no one else
will notice.

### p

I like the shape. descender looks a little bit short

### h

is it a little bit wide?

### u

good

### y

i like it. consider more weight on teardrop

### i

good.

### b

I like the b, and i like how the x-height looks. I wonder if the
spurless and serifless stem at the bottom is a good match for
the style.

### a

the teardrop is too weak. the counter should be more eggy, it
has a flat spot at the bottom.

### s

Like the capital, the top counter should be more eggy.
problematic curves on the top S stroke.

### q

fine. like p, looks liek a short descender.


### x

diagonal maybe a little bit too thick? What's with this lean?

### d

fine

### n

fine. i like the spring.

### t

i like the tail, i like the shallow scoop.

### ð

I'm really unsure what they should look like, but i guess this
is ok? cuts on terminals, of ð, t, c, j; consistent or no?

### k

I still don't think i can draw them, but this is okay.

### þ

good

### c

good. the i like the cut on the terminal.


## Critique 2021-05-13

### Y

Good. Could the basal serifs be wider?

### C

Good.
On top-curve, should thinnest part be further left?

On beak/terminal on lower part, ensure terminal is straight or slightly
tapered; avoid accidental flare.

### V

Good.

### I

Should the basal serifs be wider?

### R

The straight parts of the bowl and lower counter are boring.

### A

More pointy?

### S

Bigger beak? Problem with curve in top counter:
Not eggy enough (see _Designing_).

### H

That is a wide character.
Should bottom serifs be bigger / wider?

### D

Love the bracket on lower left part of bowl.
Should outer curved outline begin at the top from a more leftwards point?
Make more teardrop shaped.

### K

inconsistent spur at foot of leg.
Tiny serif on left of upper arm seems weird.
the top part of the leg terminal is good.

### E

It's pretty boring.
Bracket in lower left of lower counter should be more oblate.
Brackets on left should be more circular? As Y?

### Q

Should tail's terminal be similar to C?

### P

Consider bracket on lower left part of counter

### M

Replace top serifs with spurs?
Reduce left-hand serif of right-hand leg?
Top-left left-serif seems very big

### X

It leans to the right a little bit too much. Fix by moving lower left foot
to the right?

### U

Bit boring. Make inner curve of counter less semicircular, by increasing
height of spring-point.

### G

Add tiny fillet on lower right part of counter?
Consider making the area between the right-hand serif of the vertical
stroke and the spur on the lower right a single curve.

### N

Is the top serif on right leg symmetrical? reduce thickness / value on
right.

### O

Should it be very slightly prolate?

### B

Enhance teardrop / semicircle contrast? that is, more of both?
Weight / balance between top and bottom is good.

### T

Aha, those massive beaks. More weight / value on lower serifs?

### Þ

Move entire curved part down a bit?

### J

Little bit pointy at the bottom?

### W

It reminds me a lot of the waterstones logo.
Which is not exactly great, is it?

### L

Fine.

### F

Angle top-right serif / beak?

### Z

It looks like someone bent the bottom-right part.
I'm not sure i love it.

# END
